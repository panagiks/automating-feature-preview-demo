#! /bin/bash

oc project automating-feature-preview
oc delete sa/preview-sa
oc delete template/automating-feature-preview
oc delete secret/automating-feature-preview-panagiks-com
oc delete rolebinding/preview-role-sa-rb
