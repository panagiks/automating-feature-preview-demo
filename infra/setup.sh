#! /bin/bash

# oc apply -f 01-namespace.yaml
# The above should normally create a new project but there seems to be some RBAC issue
# for now, namespace created with:
# oc new-project automating-feature-preview
oc project automating-feature-preview
oc apply -f 02-service-account.yaml
# Next command assumes `key.pem` and `certificate.pem` created with:
# openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 365 -out certificate.pem
oc create secret tls automating-feature-preview-panagiks-com --cert=./certificate.pem --key=./key.pem
oc apply -f 03-app-template.yaml
